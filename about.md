---
layout: page
title: About
permalink: /about/
---

I started this project to learn more about javascript and web apps and of course
because there was no really functional way on my Firefox OS to get infos about the
Swiss public transport system.

Fortunately the guys at [opendata.ch](http://opendata.ch/) already did a tremendous
job with their [Transport API](http://transport.opendata.ch/) which enabled me to
concentrate on simply building the app without worrying to much about the data fetching
and parsing.

Take a look at the code [here](https://code.vanwa.ch/spt-fxos/app) and reach me at
[sebastian@vanwa.ch](mailto:sebastian@vanwa.ch) (find my pgp key here: [0xC84C4E44](https://static.vanwa.ch/C84C4E44.asc))
