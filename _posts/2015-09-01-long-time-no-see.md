---
layout: post
title:  "It has been a long time"
date:   2015-09-01 16:04:45
categories: spt
---

It took me a long time to get my act together and fix some annoying bugs
(I delisted the app for some time because of that), mainly in connection to the
loading screens.

If you think it looks quite different now, then you are right. Now I use [Ionic](http://ionicframework.com/)
because it works really well for simple and small apps like this (not to say it wouldn't work for bigger ones)
and also because I am just bad when it comes to visuals.

Version 0.3 should become available on the marketplace in the next few days and
with what is now some kind of semi decent foundation, I've got some ideas for the future.

Until next time.
