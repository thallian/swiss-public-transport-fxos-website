---
layout: page
---

Looking for the fastest connection with the public transport system in Switzerland? \\
Here is the app to make your life easier!

It is free as in freedom and as in beer (licensed as [MPL 2.0](https://www.mozilla.org/en-US/MPL/2.0/)) and it does not track you.

Install from the [Firefox Marketplace](https://marketplace.firefox.com/app/swiss-pt/)

![Main Screen]({{ site.url }}/assets/search.png)
![Location Screen]({{ site.url }}/assets/chooseLocation.png)
![Connections Screen]({{ site.url }}/assets/connections.png)
![Connection Screen]({{ site.url }}/assets/connection.png)
![Setting Screen]({{ site.url }}/assets/settings.png)
